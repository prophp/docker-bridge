  mysql:
    image: mysql:latest
    restart: always
    command: --default-authentication-plugin=mysql_native_password
    ports:
      - %port%:3306
    environment:
      MYSQL_DATABASE: database
      MYSQL_USER: user
      MYSQL_PASSWORD: password
      MYSQL_ROOT_PASSWORD: password
    volumes:
      - ./docker-mysql:/var/lib/mysql