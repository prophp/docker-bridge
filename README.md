# ◦ Requirements

- Linux (tested on Ubuntu 19)
- docker-compose [Install](https://docs.docker.com/compose/install/)

# ◦ Install

### ▸ `composer require prophp/docker-bridge --dev`

### ▸ `vendor/bin/docker-bridge`

## ▸ GIT commit created files

# ◦ USE

### - Docker

`docker/run`

`docker/stop` - current container

`docker/stop-all` - Stop all docker containers running in the background (current machine)


### - Gitlab CI

Rename `.gitlab-ci.ignored.yml` to `.gitlab-ci.yml` in order to enable Gitlab CI

Update `- exec "bin/test"` in `/.gitlab-ci.yml` (if required)

Insert any required command(s) instead of `bin/test`, testing purposes

__NB!!!__ Remove `/.gitlab-ci.ignored.yml` file if you do not need `Gitlab CI` completely

### Parameters

▸ Default
```shell
docker/run
```

---

▸ Apache port

Default `Apache port` is 9999

```shell
docker/run 9888
```

---

▸ Mysql

```shell
docker/run --mysql
```

By default, `mysql-port` = `Apache port` - 1

```shell
docker/run --mysql --mysql-port=9777
```

### Save default command line parameters


▸ Create `/docker/default-params.txt` and pur default command line parameters there:

```
9987 --mysql --mysql-port=9978
```

## ◦ MySQL

▸ Connect to MySQL server via terminal outside DOCKER container 

[How to Install MySQL on Linux? - GeeksforGeeks](https://www.geeksforgeeks.org/how-to-install-mysql-on-linux/)

[2.1 Installing MySQL Shell on Microsoft Windows](https://dev.mysql.com/doc/mysql-shell/8.0/en/mysql-shell-install-windows-quick.html)

```shell
mysql -u user --password=password -h 127.0.0.1 -P 9998 database
```

▸ Set up MySQL database in PHP Storm

`Database` -> `Data Source` -> `MySQL`

![alt text](README/PhpStormMysql.png "PhpStormMysql")